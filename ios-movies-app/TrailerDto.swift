 

import Foundation
import ObjectMapper

class TrailerDto: Mappable {
    var name: String!
    var size: String!
    var source: String!
    var type: String!

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        size <- map["size"]
        source <- map["source"]
        type <- map["type"]
    }
}
