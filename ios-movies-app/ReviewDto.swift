 

import Foundation
import ObjectMapper

class ReviewDto: Mappable {
    var id: String!
    var author: String!
    var content: String!
    var url: String!

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        author <- map["author"]
        content <- map["content"]
        url <- map["url"]
    }
}
